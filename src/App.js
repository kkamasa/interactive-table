// @flow
import React from 'react';
import PetList from './pets/components/List';

const App = () => (
  <div >
    <PetList />
  </div>);

export default App;
