// @flow
export const SET_FILTERS = '@pets/SET_FILTERS';
export const SET_SORT = '@pets/SET_SORT';
export const FETCH_PETS = '@pets/FETCH_PETS';
export const FETCH_PETS_SUCCESS = '@pets/FETCH_PETS_SUCCESS';
export const FETCH_PETS_ERROR = '@pets/FETCH_PETS_ERROR';
