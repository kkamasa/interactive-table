// @flow
import 'rxjs/add/operator/switchMapTo';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import { fromPromise } from 'rxjs/observable/fromPromise';
import { combineEpics, ActionsObservable } from 'redux-observable';
import { of } from 'rxjs/observable/of';
import { FETCH_PETS, FETCH_PETS_ERROR, FETCH_PETS_SUCCESS } from './actionTypes';
import { petService } from '../service';
import type { Actions } from './actions';

export const fetchPetsEpic = (action$: ActionsObservable<Actions>): ActionsObservable<Actions> =>
  action$.ofType(FETCH_PETS)
    .switchMapTo(fromPromise(petService.fetch()))
    .map(res => ({ type: FETCH_PETS_SUCCESS, payload: res }))
    .catch(() => of({ type: FETCH_PETS_ERROR }));

export default combineEpics(fetchPetsEpic);
