// @flow
import { createSelector } from 'reselect';
import type { FiltersState, Pet, SortState, State as PetState } from '../types';
import type { State } from '../../types';
import { sortDirection } from '../constants';

const baseSelector = name => (state: State): $ElementType<PetState, string> => state.pets[name];

export const petsFetchingSelector = baseSelector('fetching');
export const petsHasErrorSelector = baseSelector('hasError');
export const petsFiltersSelector = baseSelector('filters');
export const petsDataSelector = baseSelector('data');
export const petsSortSelector = baseSelector('sort');

export const petsFilteredDataSelector = createSelector(
  petsFiltersSelector,
  petsSortSelector,
  petsDataSelector,
  (filters: FiltersState, sort: SortState, data: ?Array<Pet>) =>
    data &&
        data
          .filter(item =>
            item.price < filters.minPrice &&
              (!filters.animalTypes.length || filters.animalTypes.includes(item.animal)))
          .sort((a, b) =>
            (sort.direction === sortDirection.ASC ?
              a[sort.name] - b[sort.name] : b[sort.name] - a[sort.name])),
);
