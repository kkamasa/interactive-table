// @flow
import { FETCH_PETS, SET_FILTERS, SET_SORT } from './actionTypes';
import type { SortState, FiltersState } from '../types';
import type { ExtractReturn } from '../../types';

export const setSort = (sort: SortState) => ({
  type: SET_SORT,
  payload: sort,
});

export const setFilters = (filters: FiltersState) => ({
  type: SET_FILTERS,
  payload: filters,
});

export const fetchPets = () => ({
  type: FETCH_PETS,
});

export type Actions =
    ExtractReturn<typeof setSort> |
    ExtractReturn<typeof setFilters> |
    ExtractReturn<typeof fetchPets>
