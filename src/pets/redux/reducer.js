// @flow
import type { State } from '../types';
import { FETCH_PETS, FETCH_PETS_ERROR, FETCH_PETS_SUCCESS, SET_FILTERS, SET_SORT } from './actionTypes';
import type { Actions } from './actions';
import { sortDirection } from '../constants';

const initialState: State = {
  fetching: false,
  hasError: false,
  filters: {
    minPrice: 1000,
    animalTypes: [],
  },
  sort: {
    name: 'price',
    direction: sortDirection.DESC,
  },
};

export default (state: State = initialState, action: Actions): State => {
  switch (action.type) {
    case SET_FILTERS:
      return {
        ...state,
        filters: {
          ...state.filters,
          ...action.payload,
        },
      };
    case SET_SORT:
      return {
        ...state,
        sort: action.payload,
      };
    case FETCH_PETS:
      return {
        ...state,
        data: undefined,
        fetching: true,
        hasError: false,
      };
    case FETCH_PETS_SUCCESS:
      return {
        ...state,
        data: action.payload,
        fetching: false,
        hasError: false,
      };
    case FETCH_PETS_ERROR:
      return {
        ...state,
        data: undefined,
        fetching: false,
        hasError: true,
      };
    default:
      return {
        ...state,
      };
  }
};
