// @flow
import { animalsTypes, coloursTypes, patternsTypes, sortDirection } from './constants';

export type Pet = {
    animal: $Keys<typeof animalsTypes>,
    colour: $Keys<typeof coloursTypes>,
    pattern: $Keys<typeof patternsTypes>,
    rating: number,
    price: number
}

export type FiltersState = {
    minPrice: number,
    animalTypes: Array<string>
};

export type SortState = {
    name: string,
    direction: $Keys<typeof sortDirection>,
};

export type State = {
    +fetching: boolean,
    +hasError: boolean,
    +data?: Array<Pet>,
    +filters: FiltersState,
    +sort: SortState
}
