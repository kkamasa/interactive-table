// @flow
export const sortDirection = {
  DESC: 'DESC',
  ASC: 'ASC',
};

export const animalsTypes = {
  Bird: 'Bird',
  Cat: 'Cat',
  Dog: 'Dog',
  Turtle: 'Turtle',
  Pig: 'Pig',
  Capybara: 'Capybara',
};

export const coloursTypes = {
  Black: 'Black',
  White: 'White',
  Brown: 'Brown',
  Green: 'Green',
};

export const patternsTypes = {
  Solid: 'Solid',
  Striped: 'Striped',
  Spotted: 'Spotted',
};
