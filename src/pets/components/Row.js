// @flow
import React from 'react';
import type { Pet } from '../types';

type Props = {
    pet: Pet
}

const Row = ({ pet }: Props) => (
  <tr>
    <td>{pet.animal}</td>
    <td>{pet.colour}</td>
    <td>{pet.pattern}</td>
    <td>{pet.rating}</td>
    <td>{pet.price}</td>
  </tr>
);

export default Row;
