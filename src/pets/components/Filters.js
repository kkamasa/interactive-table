// @flow
import React from 'react';
import { animalsTypes } from '../constants';
import type { FiltersState } from '../types';

type Props = {
    setFilters: Function,
    filters: FiltersState
}

const getValues = <T:Object>(o: T): Array<$Values<T>> =>
  Object.values(o);

const Filters = ({ setFilters, filters }: Props) => (
  <div>
    <label htmlFor="price">
            Price: {filters.minPrice}
      <input
        onChange={e => setFilters({ minPrice: parseInt(e.target.value, 10) })}
        type="range"
        value={filters.minPrice}
        min={0}
        max={1000}
        id="price"
      />
    </label>
    <br />
    {getValues(animalsTypes).map(type => (
      <label key={type} htmlFor={type}>
        <input
          defaultChecked={filters.animalTypes.includes(type)}
          onChange={e => setFilters({
                        animalTypes: e.target.checked ? [
                            ...filters.animalTypes, type,
                        ] : filters.animalTypes.filter(val => val !== type),
                    })}
          type="checkbox"
          id={type}
        />
        {type}
      </label>))}
  </div>);

export default Filters;
