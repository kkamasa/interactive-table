// @flow
import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import type { Pet, FiltersState, SortState } from '../types';
import Row from './Row';
import Filters from './Filters';
import { sortDirection } from '../constants';
import { fetchPets, setFilters, setSort } from '../redux/actions';
import {
  petsFetchingSelector, petsFilteredDataSelector, petsFiltersSelector,
  petsHasErrorSelector, petsSortSelector,
} from '../redux/selectors';

type Props = {
    fetchPets: () => void,
    setFilters: (filters: FiltersState) => void,
    setSort: typeof setSort,
    filters: FiltersState,
    sort: SortState,
    fetching: boolean,
    hasError: boolean,
    pets: ?Array<Pet>
}

class List extends Component<Props> {
  componentWillMount() {
    this.props.fetchPets();
  }

  render() {
    const {
      pets, fetching, hasError, filters, sort,
    } = this.props;
    const { DESC, ASC } = sortDirection;

    if (hasError) {
      return <span>Error occurred!</span>;
    }
    if (fetching || !pets) {
      return <span>Loading...</span>;
    }

    return (
      <div>
        <Filters setFilters={this.props.setFilters} filters={filters} />
        {pets.length !== 0 ?
          <table>
            <thead>
              <tr>
                <td>Animal</td>
                <td>Colour</td>
                <td>Pattern</td>
                <td>
                  <button
                    onClick={() => this.props.setSort({
                          name: 'rating',
                          direction: sort.direction === DESC ? ASC : DESC,
                      })}
                  >
                    Rating
                  </button>
                </td>
              </tr>
            </thead>
            <tbody>
              {pets.map(pet => <Row key={JSON.stringify(pet)} pet={pet} />)}
            </tbody>
          </table> : <span>No data</span>}
      </div>
    );
  }
}

const mapStateToProps = state => ({
  fetching: petsFetchingSelector(state),
  hasError: petsHasErrorSelector(state),
  pets: petsFilteredDataSelector(state),
  filters: petsFiltersSelector(state),
  sort: petsSortSelector(state),
});

const mapDispatchToProps = dispatch => bindActionCreators({
  setFilters, setSort, fetchPets,
}, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(List);
