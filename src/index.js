// @flow
import React from 'react';
import ReactDOM from 'react-dom';
import { createStore, combineReducers, applyMiddleware } from 'redux';
import { Provider } from 'react-redux';
import { createEpicMiddleware } from 'redux-observable';
import logger from 'redux-logger';
import App from './App';
import registerServiceWorker from './registerServiceWorker';
import petsReducer from './pets/redux/reducer';
import petsEpic from './pets/redux/epic';

const reducer = combineReducers({
  pets: petsReducer,
});

const middleware = applyMiddleware(
  createEpicMiddleware(petsEpic),
  logger,
);

const store = createStore(
  reducer,
  middleware,
);

ReactDOM.render(
  <Provider store={store}>
    <App />
  </Provider>,
  (document.getElementById('root'): any),
);

registerServiceWorker();
